package taiji;

import java.util.List;

enum NodeType {
  MIN,
  MAX
}

public interface AIInterface<Move, T extends AIInterface<Move, T>>{ 
  boolean isTerminal();
  List<Move> moves();
  T makeMove(Move move);
  int utility();
  boolean currentPlayer();
  NodeType nodeType();
  Object identifier();
}

interface AIEvaluator<Move, T extends AIInterface<Move, T>> {
  int evaluate(T state);
}
