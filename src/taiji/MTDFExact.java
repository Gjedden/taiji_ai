package taiji;

public class MTDFExact<M, T extends AIInterface<M, T>> {
  ABTransposition<M, T> ab = new ABTransposition<M, T>();

  public M search(T state, int utilityGuess, int maxDepth) {
    ab.resetCache();
    return searchUtilityMovePair(state, utilityGuess, maxDepth).move;
  }

  public M searchID(T state, int gameMaxDepth) {
    ab.resetCache();
    UtilityMovePair<M> best = new UtilityMovePair<M>(0, null);
    System.out.print("Depth: ");
    for(int d = 1; d < gameMaxDepth; ++d) {
      System.out.print(d);
      best = searchUtilityMovePair(state, best.utility, d);
    }
    System.out.println();
    return best.move;
  }

  public void addEvaluator(AIEvaluator<M, T> eval) {
    this.ab.addEvaluator(eval);
  }

  UtilityMovePair<M> searchUtilityMovePair(T state, int utilityGuess, int maxDepth) {
    UtilityMovePair<M> best = new UtilityMovePair<M>(utilityGuess, null);
    int upperbound = Integer.MAX_VALUE;
    int lowerbound = Integer.MIN_VALUE;

    while(lowerbound < upperbound) {
      System.out.print(".");
      int beta = best.utility;
      if(best.utility == lowerbound) {
        ++beta;
      }
      ab.reset(state);
      best = ab.decision(state, beta - 1, beta, maxDepth);
      if (best.utility < beta)
        upperbound = best.utility;
      else
        lowerbound = best.utility;
    }
    return best;
  }
}
