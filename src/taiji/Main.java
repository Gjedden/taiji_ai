package taiji;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
  public static void main(String[] args) {
    try { go(); } catch (Exception e) {e.printStackTrace(); System.exit(-1);}
    //These methods were used to test the program. All of them are very long running
    //selfPlayMTDF();

    // This is the comparison on the 5x5 board with AlphaBeta
    //fiveCompare();

    // This is the comparison on the 11x11 board with evaluators
    //evaluatorCompare();
  }

  public static void selfPlayMTDF() {
    int size = 11;
    var t = new Taiji(size, 3);
    var ai = new MTDF<TaijiMove, Taiji>();
    ai.addEvaluator(new Rand(100));
    while(!t.isTerminal()) {
      long start = System.nanoTime();
      TaijiMove m;
      m = ai.searchID(t, (size * size + 1) / 2);
      long elapsedTime = System.nanoTime() - start;
      System.out.println("Calculation time: " + elapsedTime / 1000000);
      System.out.println("Number of possible moves at node: " + t.moves().size());
      t = t.makeMove(m);
      System.out.println("Utility: " + t.utility());
      System.out.println("Move: " + m.x + " " + m.y + " " + m.r);
      t.printBoard();
    }
  }

  public static void evaluatorCompare() {
    var size = 11;
    var t = new Taiji(size, 3);
    var samples = 10;

    var mtdf = new MTDF<TaijiMove, Taiji>();

    mtdf.addEvaluator(new RandMinMax(1));
    for(int i = 0; i < samples; ++i) {
      mtdf.searchID(t, (size * size + 1) /2);
    }
    System.out.println("MINMAX: " + mtdf.ab.counter / samples);

    mtdf.ab.counter = 0;
    mtdf.addEvaluator(new RandAvg(100));
    for(int i = 0; i < samples; ++i) {
      mtdf.searchID(t, (size * size + 1) /2);
    }
    System.out.println("RANDAvg: " + mtdf.ab.counter / samples);


    mtdf.ab.counter = 0;
    mtdf.addEvaluator(new Rand(100));
    for(int i = 0; i < samples; ++i) {
      mtdf.searchID(t, (size * size + 1) /2);
    }
    System.out.println("RAND: " + mtdf.ab.counter / samples);

    mtdf.ab.counter = 0;
    mtdf.addEvaluator(new EUtility());
    for(int i = 0; i < samples; ++i) {
      mtdf.searchID(t, (size * size + 1) /2);
    }
    System.out.println("UTILITY: " + mtdf.ab.counter / samples);
  }

  public static void fiveCompare() {
    var size = 5;
    var t = new Taiji(size, 1);

    {
      var ab = new AlphaBeta<TaijiMove, Taiji>();
      ab.search(t);
      System.out.println("AB:    " + ab.counter);
    }

    {
      var abt = new ABTransposition<TaijiMove, Taiji>();
      abt.search(t);
      System.out.println("ABT:   " + abt.counter);
    }

    {
      var abtb = new ABTransBounds<TaijiMove, Taiji>();
      abtb.search(t);
      System.out.println("ABTB:  " + abtb.counter);
    }

    {
      var mtd = new MTDF<TaijiMove, Taiji>();
      mtd.search(t, 0, (size * size + 1) / 2);
      System.out.println("MTDF:  " + mtd.ab.counter);
    }

    {
      var mtde = new MTDFExact<TaijiMove, Taiji>();
      mtde.search(t, 0, (size * size + 1) / 2);
      System.out.println("MTDFE: " + mtde.ab.counter);
    }

    {
      var mtdff = new MTDFFull<TaijiMove, Taiji>();
      mtdff.search(t, 0, (size * size + 1) / 2);
      System.out.println("MTDFFull: " + mtdff.ab.counter);
    }

    {
      var mtdfeval = new MTDF<TaijiMove, Taiji>();
      mtdfeval.addEvaluator(new Rand(100));
      mtdfeval.searchID(t, (size * size + 1) / 2);
      System.out.println("MTDFEval: " + mtdfeval.ab.counter);
    }
  }

  public static void go() throws IOException {
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    while (true) {
      int size;
      int scoreGroups;
      boolean userTurn;

      System.out.println("Determine board size, value must be between 4 and 20.");
      while (true) {
        try {
          size = Integer.parseInt(reader.readLine());
          if (4 <= size && size <= 20) break;
          System.out.println("The value was not in the correct range.");
        }
        catch (IOException e) { System.out.println("Input was not accepted, try again."); }
      }

      System.out.println("Determine the amount of score groups, value must be at least 1");
      while (true) {
        try {
          scoreGroups = Integer.parseInt(reader.readLine());
          if (scoreGroups >= 1) break;
          System.out.println("The value was not at least one.");
        } catch (IOException e) { System.out.println("Input was not accepted, try again."); }
      }

      System.out.println("Do you want to start (y/n)?");
      while (true) {
        try {
          String r = reader.readLine().toLowerCase();
          if (r.equals("y") || r.equals("n")) {
            userTurn = r.equals("y");
            break;
          }
          System.out.println("Response was not in correct format.");
        }
        catch (IOException e) { System.out.println("Input was not accepted, try again."); }
      }

      var taijiGame = new Taiji(size, scoreGroups);
      var ai = new MTDF<TaijiMove, Taiji>();
      ai.addEvaluator(new Rand(100));

      if (userTurn) taijiGame.printBoard();

      while (!taijiGame.isTerminal()) {
        if (!userTurn) { // AI turn
          var aiMove = ai.searchID(taijiGame, (size * size + 1) / 2);
          taijiGame = taijiGame.makeMove(aiMove);
          System.out.println("Utility: " + taijiGame.utility());
          System.out.println("Move: " + aiMove.x + " " + aiMove.y + " " + aiMove.r);
        }
        else { // User turn
          int x, y;
          Rotation r = null;
          while (true) {
            try {
              String[] s = reader.readLine().split(" ");
              if (s.length != 3) {
                System.out.println("Wrong number of arguments, try again.");
                continue;
              }
              x = Integer.parseInt(s[0]);
              y = Integer.parseInt(s[1]);

              switch (s[2].toLowerCase()) {
                case "n":
                  r = Rotation.NORTH;
                  break;
                case "e":
                  r = Rotation.EAST;
                  break;
                case "s":
                  r = Rotation.SOUTH;
                  break;
                case "w":
                  r = Rotation.WEST;
                  break;
                default:
                  System.out.println("The given rotation was invalid.");
              }

              if (r != null && taijiGame.validMove(x, y, r)) break;
              System.out.println("The move was probably invalid, try again.");
            }
            catch (IOException e) { System.out.println("No"); }
          }

          taijiGame = taijiGame.makeMove(new TaijiMove(x, y, r));
        }
        userTurn = !userTurn;
        taijiGame.printBoard();
      } // !taijiGame.isTerminal()

      var score = taijiGame.getScore();

      boolean whiteWinner = score[0] > score[1];

      System.out.printf("The winner is %s!\n", whiteWinner ? "White" : "Black");
      System.out.printf("The score is:\nWhite  :  %d\nBlack  :  %d\n", score[0], score[1]);

      System.out.println("\n\n\nPress Enter to start a new game.");
      reader.readLine();
    }
  }
}
