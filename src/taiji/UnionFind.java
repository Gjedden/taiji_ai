package taiji;

import java.util.TreeSet;
import java.util.List;
import java.util.stream.Collectors;

public class UnionFind implements Cloneable{
  private int[] size;
  private int[] id;
  private TreeSet<Integer> sets;

  public UnionFind(int size) {
    sets = new TreeSet<Integer>();
    this.size = new int[size];
    this.id = new int[size];
    for(int i = 0; i < size; i++) {
      id[i] = i;
      this.size[i] = 1;
    }
  }

  public int find(int key) {
    if (id[key] != key) {
      id[key] = find(id[key]);
    }
    return id[key];
  }

  public void union(int key1, int key2) {
    var i = find(key1);
    var j = find(key2);
    if (i != j ) {
      size[i] += size[j];
      size[j] = 0;
      sets.remove(j);
      id[j] = i;
    }
  }

  public void addToSet(int key) {
    sets.add(key);
  }

  public List<Integer> sizes() {
    return sets.stream()
               .map(s -> size[s]).collect(Collectors.toList());
  }

  @Override
  protected Object clone() throws CloneNotSupportedException {
    var cloned = (UnionFind) super.clone();
    cloned.size = size.clone();
    cloned.id = id.clone();
    cloned.sets = new TreeSet<Integer>(sets);
    return cloned;
  }
}
