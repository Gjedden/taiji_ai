package taiji;
import java.util.HashMap;

class TranspositionNode {
  int lowerbound, upperbound;

  public TranspositionNode() {
    lowerbound = Integer.MIN_VALUE;
    upperbound = Integer.MAX_VALUE;
  }
}

public class ABTransBounds<M, T extends AIInterface<M, T>> extends AlphaBeta<M, T> {
  protected HashMap<Object, TranspositionNode> transpositionTable = new HashMap<Object, TranspositionNode>();

  public M search(T state, int maxDepth) {
    reset(state);
    return super.search(state, maxDepth);
  }

  public void resetCache() {
    transpositionTable = new HashMap<Object, TranspositionNode>();
  }

  public void reset(T state) {
    transpositionTable.remove(state.identifier());
  }

  public UtilityMovePair<M> decision(T state, int alpha, int beta, int depth) {
    var node = getNode(state.identifier());
    if(node.lowerbound >= beta)
      return new UtilityMovePair<M>(node.lowerbound, null);
    if(node.upperbound <= alpha)
      return new UtilityMovePair<M>(node.upperbound, null);
    beta = Math.min(beta, node.upperbound);
    alpha = Math.max(alpha, node.lowerbound);

    var utilityMove = super.decision(state, alpha, beta, depth);

    var utility = utilityMove.utility;
    if(utility <= alpha)
      node.upperbound = utility;
    if(utility >= beta)
      node.lowerbound = utility;

    return utilityMove;
  }

  TranspositionNode getNode(Object identifier) {
    TranspositionNode node;
    if (transpositionTable.containsKey(identifier)) {
      node = transpositionTable.get(identifier);
    }
    else {
      node = new TranspositionNode();
      transpositionTable.put(identifier, node);
    }
    return node;
  }
}
