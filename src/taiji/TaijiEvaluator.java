package taiji;

import java.util.stream.*;
import java.util.Random;
import java.util.List;


// On min node, randomly uses one of the moves that minimises the current utility
// On max node does the opposite
// Until the state is terminal
class RandMinMax implements AIEvaluator<TaijiMove, Taiji> {
  int samples;
  int counter = 0;
  Random rand = new Random(); 

  public RandMinMax(int samples) {
    this.samples = samples;
  }

  public int evaluate(Taiji state) {
    counter++;
    if(counter % 1000 == 0)
      System.out.print("s");
    int best = evaluateInternal(state);
    for(int i = 1; i < samples; ++i) {
      if(state.currentPlayer()) {
        best = Math.min(evaluateInternal(state), best);
      } else {
        best = Math.max(evaluateInternal(state), best);
      }
    }
    return best;
  }

  int evaluateInternal(Taiji state) {
    if (state.isTerminal()) {
      return state.utility();
    }
    List<Taiji> children = state.moves().stream().map(m -> state.makeMove(m)).collect(Collectors.toList());
    int matchValue;
    if (state.currentPlayer()) {
      // Min node
      matchValue = Integer.MAX_VALUE;
      for(var s : children) {
        if(s.utility() < matchValue) {
          matchValue = s.utility();
        }
      }
    } else {
      // Max node
      matchValue = Integer.MIN_VALUE;
      for(var s : children) {
        if(s.utility() > matchValue) {
          matchValue = s.utility();
        }
      }
    }
    final int value = matchValue;
    children = children.stream().filter(s -> s.utility() == value).collect(Collectors.toList());
    return evaluateInternal(children.get(rand.nextInt(children.size()))); 
  }
}

class Rand implements AIEvaluator<TaijiMove, Taiji> {
  int samples;
  int counter = 0;
  Random rand = new Random(); 

  public Rand(int samples) {
    this.samples = samples;
  }

  public int evaluate(Taiji state) {
    counter++;
    if(counter % 1000 == 0)
      System.out.print("s");
    int best = evaluateInternal(state);
    for(int i = 1; i < samples; ++i) {
      if(state.nodeType() == NodeType.MIN) {
        best = Math.min(evaluateInternal(state), best);
      } else {
        best = Math.max(evaluateInternal(state), best);
      }
    }
    return best;
  }

  int evaluateInternal(Taiji state) {
    if (state.isTerminal()) {
      return state.utility();
    }
    var moves = state.moves();
    return evaluateInternal(state.makeMove(moves.get(rand.nextInt(moves.size())))); 
  }
}

class Utility implements AIEvaluator<TaijiMove, Taiji> {
  public Utility() {
  }

  public int evaluate(Taiji state) {
    return state.utility();
  }
}

class RandAvg implements AIEvaluator<TaijiMove, Taiji> {
  int samples;
  int counter = 0;
  Random rand = new Random(); 

  public RandAvg(int samples) {
    this.samples = samples;
  }

  public int evaluate(Taiji state) {
    counter++;
    if(counter % 1000 == 0)
      System.out.print("s");
    int sum = evaluateInternal(state);
    for(int i = 1; i < samples; ++i) {
      if(state.nodeType() == NodeType.MIN) {
        sum += evaluateInternal(state);
      } else {
        sum += evaluateInternal(state);
      }
    }
    return sum / samples;
  }

  int evaluateInternal(Taiji state) {
    if (state.isTerminal()) {
      return state.utility();
    }
    var moves = state.moves();
    return evaluateInternal(state.makeMove(moves.get(rand.nextInt(moves.size())))); 
  }
}

class EUtility implements AIEvaluator<TaijiMove, Taiji> {
  public EUtility() {
  }

  public int evaluate(Taiji state) {
    return state.utility();
  }
}
