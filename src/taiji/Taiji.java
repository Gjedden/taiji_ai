package taiji;

import java.util.Arrays;
import java.util.List;
import java.util.Collections;
import java.util.ArrayList;


enum Rotation {
  NORTH,
  EAST,
  SOUTH,
  WEST
}

class TaijiMove {
  protected int x, y;
  protected Rotation r;

  public TaijiMove(int x, int y, Rotation r) {
    this.x = x;
    this.y = y;
    this.r = r;
  }
}

class TaijiIdentifier {
  private long[] tiles;

  public TaijiIdentifier(Tile[][] tiles) {
    this.tiles = generateCompressedRepresentation(tiles);

    for(int i = 0; i < 4; ++i) {
      rotate(tiles);
      var v = generateCompressedRepresentation(tiles);
      if(worseThan(v))
        this.tiles = v;
    }
  }

  private long[] generateCompressedRepresentation(Tile[][] t) {
    int rowsPerLong = 64 / (2 * t.length);
    long[] res = new long[(t.length - 1 + rowsPerLong) / rowsPerLong];
    for (var v: res) {
      assert v == 0;
    }
    for(int i = 0; i < t.length; ++i) {
      var rowShift = t.length * (i % rowsPerLong);
      for(int j = 0; j < t[i].length; ++j) {
        var colShift = j;
        var mask = ((long) t[i][j].ordinal()) << (2L * (j + t.length * (i % rowsPerLong)));
        assert t[i][j].ordinal() <= 2 && t[i][j].ordinal() >= 0;
        assert (t[i][j].ordinal() | 0b11) == 0b11;
        assert 2*(j + t.length * (i % rowsPerLong)) <= 63;
        assert (res[i/rowsPerLong] & mask) == 0;
        res[i/rowsPerLong] |= mask;
      }
    }
    return res;
  }

  private boolean worseThan(long[] t) {
    for(int i = 0; i < tiles.length; ++i) {
      if (tiles[i] < t[i])
        return true;
      if (tiles[i] > t[i])
        return false;
    }
    return false;
  }

  private void rotate(Tile[][] tiles) {
    int n = tiles.length / 2;
    for (int x = 0; x < n; x++) {
      for (int y = x; y < n-x-1; y++) {
        var temp = tiles[x][y];
        tiles[x][y] = tiles[y][n-1-x];
        tiles[y][n-1-x] = tiles[n-1-x][n-1-y];
        tiles[n-1-x][n-1-y] = tiles[n-1-y][x];
        tiles[n-1-y][x] = temp;
      }
    }
  }

  public boolean equals(Object o) {
    if(o == null){
      return false;
    }
    if (o == this) {
      return true;
    }
    if (getClass() != o.getClass()) {
      return false;
    }

    TaijiIdentifier t = (TaijiIdentifier) o;
    
    return Arrays.equals(tiles, t.tiles);
  }

  public int hashCode() {
    return Arrays.hashCode(tiles);
  }
}

public class Taiji implements AIInterface<TaijiMove, Taiji>, Cloneable {
  static private final Rotation[] rotations = new Rotation[]{Rotation.NORTH, Rotation.EAST, Rotation.SOUTH, Rotation.WEST};
  private int n;
  private int scoreGroups;
  private boolean turn = false;
  private Tile[][] tiles;
  private UnionFind blackGroups;
  private UnionFind whiteGroups;
  private ArrayList<TaijiMove> cachedMoves;

  public Taiji(int n, int scoreGroups) {
    var size = n * n;

    this.n = n;
    this.scoreGroups = scoreGroups;
    blackGroups = new UnionFind(size);
    whiteGroups = new UnionFind(size);

    this.tiles = new Tile[n][n];
    for(int x = 0; x < n; x++) {
      for(int y = 0; y < n; y++) {
        tiles[x][y] = Tile.EMPTY;
      }
    }
  }

  public boolean isTerminal() {
    return moves().isEmpty();
  }

  public List<TaijiMove> moves() {
    if (cachedMoves == null) {
      cachedMoves = new ArrayList<TaijiMove>();
      for(int x = 0 ; x < n; x++) {
        for(int y = 0; y < n; y++) {
          for(Rotation r: rotations) {
            if(validMove(x, y, r))
              cachedMoves.add(new TaijiMove(x, y, r));
          }
        }
      }
    }
    return cachedMoves;
  }

  public Taiji makeMove(TaijiMove move) {
    try {
      var child = (Taiji) this.clone();
      child.placeTile(move.x, move.y, move.r);
      // Removes invalid moves from child state
      child.cachedMoves.removeIf(childMove -> !child.validMove(childMove.x, childMove.y, childMove.r));
      return child;
    } catch (Exception e) {
      e.printStackTrace(System.err);
    }
    return null;
  }

  public int utility() {
    return playerScore(whiteGroups) - playerScore(blackGroups);
  }

  public int[] getScore() {
      return new int[] { playerScore(whiteGroups), playerScore(blackGroups) };
  }

  public boolean currentPlayer() {
    return turn;
  }

  public NodeType nodeType() {
    if(turn) { // black player
      return NodeType.MIN;
    } else { // white player
      return NodeType.MAX;
    }
  }

  public void placeTile(int wx, int wy, Rotation r) {
    int bx = wx;
    int by = wy;
    switch(r) {
      case NORTH: 
        by++;
      break;
      case EAST:
        bx++;
      break;
      case SOUTH:
        by--;
      break;
      case WEST:
        bx--;
      break;
    }
    if (!(onBoard(wx,wy) && isEmpty(wx, wy) && onBoard(bx, by) && isEmpty(bx, by))) {
      throw new IllegalArgumentException("Invalid move!");
    }

    setTile(wx, wy, Tile.WHITE);
    whiteGroups.addToSet(wx * n + wy);
    checkNeighbors(wx, wy, whiteGroups);

    setTile(bx, by, Tile.BLACK);
    blackGroups.addToSet(bx * n + by);
    checkNeighbors(bx, by, blackGroups);

    turn = !turn;
  }

  public Tile getTile(int x, int y) {
    return tiles[n-y - 1][x];
  }

  public Object identifier() {
    return new TaijiIdentifier(tiles);
  }

  void setTile(int x, int y, Tile t) {
    tiles[n-y - 1][x] = t;
  }

  boolean validMove(int wx, int wy, Rotation r) {
    int bx = wx;
    int by = wy;
    switch(r) {
      case NORTH: 
        by++;
      break;
      case EAST:
        bx++;
      break;
      case SOUTH:
        by--;
      break;
      case WEST:
        bx--;
      break;
    }
    return onBoard(wx,wy) && isEmpty(wx, wy) && onBoard(bx, by) && isEmpty(bx, by);
  }

  boolean onBoard(int x, int y) {
    return !(x < 0 || x >= n || y < 0 || y >= n);
  }

  boolean isEmpty(int x, int y) {
    return getTile(x, y) == Tile.EMPTY;
  }

  void checkNeighbors(int x, int y, UnionFind g) {
    for(int dx = -1; dx <= 1; ++dx) {
      for(int dy = -1; dy <= 1; ++dy) {
        if (dx != 0 && dy != 0)
          continue;
        if(onBoard(dx +  x, dy + y) && getTile(x, y) == getTile(dx + x, dy + y)) {
          g.union(x*n + y, (dx + x)*n + dy + y);
        }
      }
    }
  }
  
  int playerScore(UnionFind g) {
    List<Integer> groups = g.sizes();
    Collections.sort(groups, Collections.reverseOrder());
    int score = 0;
    for(int i = 0; i < groups.size() && i < scoreGroups; ++i) {
      score += groups.get(i);
    }
    return score;
  }

  @Override
  @SuppressWarnings("unchecked")
  protected Object clone() throws CloneNotSupportedException {
    var cloned = (Taiji) super.clone();
    cloned.tiles = Arrays.stream(tiles).map(row -> row.clone()).toArray(p -> tiles.clone());
    cloned.blackGroups = (UnionFind) blackGroups.clone();
    cloned.whiteGroups = (UnionFind) whiteGroups.clone();
    cloned.cachedMoves = (ArrayList<TaijiMove>) cachedMoves.clone();
    return cloned;
  }

  public void printBoard() {
    for(var row : tiles) {
      for (var tile : row) {
        if(tile == Tile.EMPTY)
          System.out.print("O");
        if(tile == Tile.WHITE)
          System.out.print("W");
        if(tile == Tile.BLACK)
          System.out.print("B");
      }
      System.out.println();
    }
    System.out.println();
  }
}
