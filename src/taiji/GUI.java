package taiji;

import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class GUI {
  private JFrame window;
  private int size = 11;
  private int groups = 3;
  private boolean starting = true;
  private String type = "AI";

  private int placedTiles = 0;
  private int wx, wy, bx, by; // location of placed tiles
  private int[][] selected = null;
  private JButton[][] buttonGrid = null;

  private JPanel board = null;
  private JPanel log = null;
  private JPanel interactButtons = null;

  private Taiji game = null;
  private MTDF<TaijiMove, Taiji> ai = new MTDF<TaijiMove, Taiji>(); 

  final Color EMPTY = new Color(100, 100, 100), WHITE = new Color(255,255,255), BLACK = new Color(0,0,0), WHITESELECT = new Color(180,180,180), BLACKSELECT = new Color(50,50,50);

  public static void main(String[] args) {

    GUI g = new GUI(); 
    g.ai.addEvaluator(new Rand(100));
    g.prepareGUI();
    g.menu();
  }

  public void prepareGUI() {
    window = new JFrame("Taiji");
    window.setSize(800,800);
    window.setResizable(false);
    window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    window.setVisible(true);
    window.setLayout(null);
  }

  public void menu() {
    JPanel panel = new JPanel();
    panel.setBounds(0,0,800,800);
    panel.setLayout(null);

    //var aiButton = new JButton("AI VS AI");
    //aiButton.setBounds(300, 250, 200, 100);
    //aiButton.addActionListener(new ActionListener() {
    //  public void actionPerformed(ActionEvent e) {
    //    removePanel(panel);
    //    type = "AI";
    //    gameTypeMenu();
    //  }
    //});
    //panel.add(aiButton);

    var box = new JCheckBox("Starting", true);
    box.setBounds(600,450, 100, 100);
    panel.add(box);

    var playerButton = new JButton("PLAYER VS AI");
    playerButton.setBounds(300, 450, 200, 100);
    playerButton.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        removePanel(panel);
        type = "PLAYER";
        starting = box.isSelected();
        gameTypeMenu();
      }
    });

    panel.add(playerButton);
    window.add(panel);
  }

  private void removePanel(JPanel p) {
    p.invalidate();
    p.setVisible(false);
    p.removeAll();
    window.getContentPane().remove(p);
  }

  public void gameTypeMenu() {
    JPanel panel = new JPanel();
    panel.setBounds(0,0,800,800);
    panel.setLayout(null);

    JSpinner sizeModel = new JSpinner(new SpinnerNumberModel(9, 4, 20, 1));
    sizeModel.setBounds(200,250,100,50);
    JSpinner groupModel = new JSpinner(new SpinnerNumberModel(3, 1, 3, 1));
    groupModel.setBounds(200,450,100,50);

    JLabel sizeLabel = new JLabel("Size");
    sizeLabel.setBounds(100, 250, 100, 50);
    JLabel groupLabel = new JLabel("Scoring groups");
    groupLabel.setBounds(100, 450, 100, 50);
    panel.add(sizeLabel);
    panel.add(groupLabel);

    panel.add(sizeModel);
    panel.add(groupModel);

    var play = new JButton("Play");

    play.addActionListener(new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        size = (Integer) sizeModel.getValue();
        groups = (Integer) groupModel.getValue();
        removePanel(panel);
        startGame();
      }
    });

    panel.add(play);
    play.setBounds(400, 600, 200, 100);

    window.add(panel);
  }

  private void updateBoard(Taiji state) {
    placedTiles = 0;
    for(int y = size - 1; y >= 0; --y) {
      for(int x = 0; x < size; ++x) {
        switch(state.getTile(x, y)) {
          case BLACK:
            selected[y][x] = -1;
            buttonGrid[y][x].setBackground(BLACK);
            break;
          case WHITE:
            selected[y][x] = -1;
            buttonGrid[y][x].setBackground(WHITE);
            break;
          case EMPTY:
            selected[y][x] = 0;
            buttonGrid[y][x].setBackground(EMPTY);
            break;
        }
      }
    }
  }

  private void startGame() {
    System.out.println("Starting game with size: " + size + ", scoring groups: " + groups + ", Player starting: " + starting);

    game = new Taiji(size, groups);

    selected = new int[size][size];
    buttonGrid = new JButton[size][size];

    board = new JPanel();
    board.setBounds(0,0,500,500);
    board.setLayout(null);

    board.setLayout(new GridLayout(size, size, 2, 2));

    for(int y = size - 1; y >= 0; --y) {
      for(int x = 0; x < size; ++x) {
        var b = new JButton();
        buttonGrid[y][x] = b;
        b.setBackground(EMPTY);
        b.setOpaque(true);
        b.setBorderPainted(false);
        final int xx = x, yy = y;
        b.addActionListener(new ActionListener() {
          public void actionPerformed(ActionEvent e) {
            if(selected[yy][xx] == 0 && placedTiles == 0) {
              b.setBackground(WHITESELECT);
              selected[yy][xx] = 1;
              placedTiles = 1;
              wx = xx;
              wy = yy;
            }
            else if(selected[yy][xx] == 0 && placedTiles == 1) {
              b.setBackground(BLACKSELECT);
              selected[yy][xx] = 2;
              placedTiles = 2;
              bx = xx;
              by = yy;
            }
            else if(selected[yy][xx] == 2 && placedTiles == 2) {
              b.setBackground(EMPTY);
              selected[yy][xx] = 0;
              placedTiles = 1;
            }
            else if(selected[yy][xx] == 1 && placedTiles == 1) {
              b.setBackground(EMPTY);
              selected[yy][xx] = 0;
              placedTiles = 0;
            }
          }
        });
        board.add(b);
      }
    }

    
    interactButtons = new JPanel();
    interactButtons.setBounds(0,500,300,300);
    interactButtons.setVisible(false);
    window.add(interactButtons);
    
    if(type.equals("PLAYER")) {
      var play = new JButton("Play move");
      play.setForeground(Color.WHITE);
      play.addActionListener(new ActionListener() {
        public void actionPerformed(ActionEvent e) {
          handlePlay();
        }
      });
      interactButtons.add(play);
    }

    window.add(board);

    gameLoop();
  }

  private void gameLoop() {
    updateBoard(game);
    System.out.println("Current utility: " + game.utility());
    if(game.nodeType() == NodeType.MIN) {
      System.out.println("Black turn (minimize)");
    }
    else {
      System.out.println("White turn (maximize)");
    }
    if(game.isTerminal()) {
    }
    else if(starting != game.currentPlayer()) {
      interactButtons.setVisible(true);
    }
    else {
      var m = ai.searchID(game, (size * size + 1) / 2);
      game = game.makeMove(m);
      gameLoop();
      buttonGrid[m.y][m.x].setBackground(new Color(150, 150, 255));
      JButton black =  null;
      switch(m.r) {
        case NORTH:
          black = buttonGrid[m.y + 1][m.x];
          break;
        case SOUTH:
          black = buttonGrid[m.y - 1][m.x];
          break;
        case WEST:
          black = buttonGrid[m.y][m.x - 1];
          break;
        case EAST:
          black = buttonGrid[m.y][m.x + 1];
          break;
      }
      black.setBackground(new Color(0,0,100));
    }
  }

  private void handlePlay() {
    interactButtons.setVisible(false);
    if(placedTiles == 2 && Math.abs(wx - bx) + Math.abs(wy - by) == 1) {
      TaijiMove m = null;
      if(wy + 1 == by) {
        m = new TaijiMove(wx, wy, Rotation.NORTH);
      } else if (wy - 1 == by) {
        m = new TaijiMove(wx, wy, Rotation.SOUTH);
      } else if (wx - 1 == bx) {
        m = new TaijiMove(wx, wy, Rotation.WEST);
      } else {
        m = new TaijiMove(wx, wy, Rotation.EAST);
      }
      game = game.makeMove(m);
      gameLoop();
      return;
    }
    interactButtons.setVisible(true);
  }
}
