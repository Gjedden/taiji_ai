package taiji;

class UtilityMovePair<M> implements Comparable<UtilityMovePair<M>> {
  int utility;
  M move;

  public UtilityMovePair(int utility, M move) {
    this.utility = utility;
    this.move = move;
  }

  public int compareTo(UtilityMovePair<M> um) {
    if(utility == um.utility)
      return 0;
    else if (utility < um.utility)
      return 1;
    return -1;
  }
}

public class AlphaBeta<M, T extends AIInterface<M, T>> {
  // Statistics
  long counter = 0;
  float timeMillis = 0.0f;
  AIEvaluator<M, T> eval = null;

  public void addEvaluator(AIEvaluator<M, T> eval) {
    this.eval = eval;
  }

  public M search(T state) {
    return search(state, Integer.MAX_VALUE);
  }

  public M search(T state, int maxDepth) {
    UtilityMovePair<M> p;
    if (state.isTerminal()) {
      throw new IllegalArgumentException("State cannot begin a search on a terminal state!");
    }
    var start = System.currentTimeMillis();
    var m = decision(state, Integer.MIN_VALUE, Integer.MAX_VALUE, maxDepth).move;
    timeMillis += System.currentTimeMillis() - start;
    return m;
  }

  public float averageStatesPerSecond() {
    return counter / (timeMillis / 1000);
  }

  UtilityMovePair<M> decision(T state, int alpha, int beta, int depth) {
    counter += 1;
    if (state.isTerminal()) { // Terminal state and we can check the utility
      int utility = state.utility();
      return new UtilityMovePair<M>(utility, null);
    }
    if(depth == 0) { // Max depth has been reached
      return new UtilityMovePair<M>(evaluate(state), null);
    }
    if(state.nodeType() == NodeType.MIN) { // Black moves
      return minDecision(state, alpha, beta, depth);
    }
    else { // White moves
      return maxDecision(state, alpha, beta, depth);
    }
  }

  int evaluate(T state) {
    if (eval == null) {
      throw new IllegalArgumentException("Cannot use depth limited search when no evaluator has been given!");
    }
    return eval.evaluate(state);
  }

  UtilityMovePair<M> maxDecision(T state, int alpha, int beta, int depth) {
    var v = new UtilityMovePair<M>(Integer.MIN_VALUE, null);
    for(var m: state.moves()) {
      var u = decision(state.makeMove(m), alpha, beta, depth - 1);
      u.move = m;
      if (v.utility < u.utility) {
        v = u;
      }
      if (v.utility >= beta) {
        return v;
      }
      alpha = Math.max(alpha, v.utility);
    }
    return v;
  }

  UtilityMovePair<M> minDecision(T state, int alpha, int beta, int depth) {
    var v = new UtilityMovePair<M>(Integer.MAX_VALUE, null);
    for(var m: state.moves()) {
      var u = decision(state.makeMove(m), alpha, beta, depth - 1);
      u.move = m;
      if (v.utility > u.utility) {
        v = u;
      }
      if (v.utility <= alpha) {
        return v;
      }
      beta = Math.min(beta, v.utility);
    }
    return v;
  }
}
