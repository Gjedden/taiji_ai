package taiji;
import java.util.HashMap;

public class ABTransposition<M, T extends AIInterface<M, T>> extends AlphaBeta<M, T> {
  protected HashMap<Object, Integer> transpositionTable = new HashMap<Object, Integer>();

  public M search(T state, int depth) {
    if(depth != Integer.MAX_VALUE) {
      throw new IllegalArgumentException("ABTransposition does not support depth limited search. Use ABTransBounds instead");
    }
    reset(state);
    return super.search(state, depth);
  }

  public M search(T state) {
    reset(state);
    return super.search(state);
  }

  public void resetCache() {
    transpositionTable = new HashMap<Object, Integer>();
  }

  public void reset(T state) {
    transpositionTable.remove(state.identifier());
  }

  UtilityMovePair<M> decision(T state, int alpha, int beta, int depth) { 
    var identifier = state.identifier();
    if (transpositionTable.containsKey(identifier)) {
      return new UtilityMovePair<M>(transpositionTable.get(identifier), null);
    }
    var utilityMove = super.decision(state, alpha, beta, depth); 
    transpositionTable.put(identifier, utilityMove.utility);
    return utilityMove;
  }
}
