# Taiji AI
There are two version of the our program, one with a text-based interface and one with a proper graphical user interface. We'd recommend running the proper GUI version.

## To run the program
1) Make sure you have the correct version of Java installed: 1.12 or higher
2) Run the program...
    1. For the text-based interface, run src/taiji/Main.java
    2. For the graphical user interface, run src/taiji/GUI.java
3) Good luck and have fun


## How to use the text-based interface
The coordinate system is zero-indexed and starts at the lower left corner and the first coordinate is the horizontal axis. When placing a piece you'll give the coordinate of the white tile and then give one of four rotations: 'n', 'e', 's' or 'w' for North, East, South or West, respectively. The input is not case sensitive.

## How to use the GUI
Just click the tiles where you want to place your piece and once you've selected a valid move you can click on the 'Play move' button to actually play your move.
